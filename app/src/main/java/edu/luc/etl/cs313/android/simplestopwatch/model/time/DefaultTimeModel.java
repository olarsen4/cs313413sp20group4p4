package edu.luc.etl.cs313.android.simplestopwatch.model.time;

import static edu.luc.etl.cs313.android.simplestopwatch.common.Constants.*;

import edu.luc.etl.cs313.android.simplestopwatch.android.StopwatchAdapter;
import edu.luc.etl.cs313.android.simplestopwatch.android.StopwatchAdapter.*;

/**
 * An implementation of the stopwatch data model.
 */
public class DefaultTimeModel implements TimeModel {

    private int runningTime = 0;
    private StopwatchAdapter sa;
    /**
     * follows project functional requirement
     *If the button is pressed when the timer is stopped, the time is incremented by one up to a preset maximum of 99. (The button acts as an increment button.)
     *
     */
   protected final int min = 0;
   protected final int max = 99;

    /**
     * sets run time to 0
     */
    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    /**
     * creates new run time after decremented run tim
     */
    @Override
    public void decRuntime() {
        sa = new StopwatchAdapter();
        runningTime = sa.getClickCount() - SEC_PER_TICK;
    }

    /**
     * run time after incrementing State
     */
    @Override
    public void incRuntime() {
        runningTime = (runningTime + SEC_PER_TICK) % SEC_PER_HOUR;
    }

    /**
     *
     * @return runningTime at any point in the machine
     */
    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override

    public boolean ifHighest()
    {
        return runningTime>= max;
    }/**
     if empty
 */
    public boolean ifNull()
    {
        return runningTime <=min;
    }

    @Override
    public boolean isFull() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}