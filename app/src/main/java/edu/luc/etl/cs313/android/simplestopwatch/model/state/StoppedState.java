package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import android.widget.TextView;

import org.w3c.dom.Text;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class StoppedState implements StopwatchState {


    public StoppedState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    /**
     * Whenever the button is pressed this is ran
     */
    @Override
    public void onStartStop() {
        sm.actionStart();
        sm.toRunningState();
    }

    /**
     * Whenever a second has passed this is ran.
     */
    @Override
    public void onTick() {
        sm.actionRingTheAlarm();
    }

    /**
     * Whenever an element changes on the screen this is ran.
     */
    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    /**
     * Returns the id of the current state
     * @return  A String
     */
    @Override
    public int getId() { return R.string.STOPPED;
    }
}
