package edu.luc.etl.cs313.android.simplestopwatch.model.time;

/**
 * The passive data model of the stopwatch.
 * It does not emit any events.
 *
 * @author laufer
 */
public interface TimeModel {
    void resetRuntime();
    void decRuntime();
    void incRuntime();

    public int getRuntime();
   // void setLaptime(); we aren't worrying about laps.
    //int getLaptime();
    boolean ifHighest();
    boolean ifNull();

    boolean isFull();

    boolean isEmpty();
}
