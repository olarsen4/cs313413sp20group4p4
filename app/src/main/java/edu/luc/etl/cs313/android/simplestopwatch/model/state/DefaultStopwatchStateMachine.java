package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
public class DefaultStopwatchStateMachine implements StopwatchStateMachine {

    public DefaultStopwatchStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;

    private final ClockModel clockModel;
    int delay=0;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private StopwatchState state;

    protected void setState(final StopwatchState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
    }

    private StopwatchUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final StopwatchUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override public synchronized void onStartStop() { state.onStartStop(); }
    @Override public synchronized void onTick()      { state.onTick(); }
    @Override public void updateUIRuntime() { uiUpdateListener.updateTime(timeModel.getRuntime()); }

    // known states from diagram- reset, running, incrementing, stop
    private final StopwatchState STOPPEDRESET     = new StoppedResetState(this); // If the timer is running and the button is pressed, the timer stops and the time is reset to zero. (The button acts as a cancel button.)
    private final StopwatchState RUNNING     = new RunningState(this);
    private final StopwatchState INCREMENT = new IncrementState(this);
    private final StopwatchState STOPPED = new StoppedState(this);

    // transitions
    @Override public void toRunningState()    { setState(RUNNING); }
    @Override public void toStoppedResetState()    { setState(STOPPEDRESET); }
    @Override public void toIncrementState() { setState(INCREMENT); }
    @Override public void toStoppedState() { setState(STOPPED); }

    @Override
    public void toLapRunningState() {

    }

    // actions
    @Override public void actionInit()       { toStoppedState(); actionReset(); }
    @Override public void actionReset()      { timeModel.resetRuntime(); actionUpdateView(); }
    @Override public void actionStart()      { clockModel.start(); }
    @Override public void actionStop()       { clockModel.stop(); }

    @Override
    public void actionLap() {

    }

    @Override public void actionInc()        { timeModel.decRuntime(); delay = 0; actionUpdateView();}
    @Override public void actionUpdateView() { state.updateView(); } //updates state machine
    @Override public void actionRingTheAlarm() {uiUpdateListener.playDefaultNotification();} //rings notification
    @Override public int getDelay(){ //returns potential delay
        return delay;
    }

    @Override public void setDelay(int t) {delay = t;}

    @Override public boolean reachMax(){
        return timeModel.isFull();
    }

    @Override public boolean countedDown(){
        return timeModel.isEmpty();
    }

}
