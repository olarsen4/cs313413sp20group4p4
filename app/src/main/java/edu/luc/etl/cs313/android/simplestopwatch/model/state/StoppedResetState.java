package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class  StoppedResetState implements StopwatchState {

    public StoppedResetState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    /**
     * Whenever the button is pressed this is ran
     */
    @Override
    public void onStartStop() {
        sm.actionStart();
        sm.toLapRunningState();
    }



    /**
     * Whenever a second has passed this is ran.
     */
    @Override
    public void onTick() {
        throw new UnsupportedOperationException("onTick");
    }

    /**
     * Whenever an element changes on the screen this is ran.
     */
    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    /**
     * Returns the id of the current state
     * @return  A String
     */
    @Override
    public int getId() {
        return R.string.LAP_STOPPED;
    }
}
