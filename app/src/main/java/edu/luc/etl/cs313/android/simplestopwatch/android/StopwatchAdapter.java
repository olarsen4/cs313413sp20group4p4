package edu.luc.etl.cs313.android.simplestopwatch.android;
//comment to test push
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.Constants;
import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.ConcreteStopwatchModelFacade;
import edu.luc.etl.cs313.android.simplestopwatch.model.StopwatchModelFacade;

/**
 * A thin adapter component for the stopwatch.
 *
 * @author laufer
 */
public class StopwatchAdapter extends Activity implements StopwatchUIUpdateListener {

    private static String TAG = "stopwatch-android-activity";
    private Integer clickCount = 0;
    private TextView counter;
    private CountDownTimer countdown;

    /**
     * The state-based dynamic model.
     */
    private StopwatchModelFacade model;

    protected void setModel(final StopwatchModelFacade model) {
        this.model = model;
    }

    /**
     * <p>
     *     This plays a default notification when the functional requirement is met.
     *     This can occur either when the timer reaches zero, or the timer is stopped for three seconds.
     * </p>
     */
    public void playDefaultNotification() {
        // Code was taken from the Clicker Application
        final Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final MediaPlayer mediaPlayer = new MediaPlayer();
        final Context context = getApplicationContext();

        try {
            mediaPlayer.setDataSource(context, defaultRingtoneUri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            mediaPlayer.start();
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * <p>This is run once the application overlay is created. Initializes several models.</p>
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // inject dependency on view so this adapter receives UI events
        setContentView(R.layout.activity_main);
        // inject dependency on model into this so model receives UI events
        this.setModel(new ConcreteStopwatchModelFacade());
        // inject dependency on this into model to register for UI updates
        model.setUIUpdateListener(this);
    }

    /**
     * Creates the options menu and performs actions after it has been created.
     * @param menu  An object referencing the menu bar/ symbol
     * @return  A boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    /**
     * Code that is executed upon the application started.
     */
    @Override
    protected void onStart() {
        super.onStart();
        model.onStart();


        countdown = new CountDownTimer(3000, 1000) {
            /**
             * Runs once per tick.
             * This is once per second.
             * @param l The tick length
             */
            @Override
            public void onTick(long l) {

            }

            /**
             * Once millisInFuture is met the code completes and updates.
             */
            @Override
            public void onFinish() {
                clickCount = 0;
                updateTime(clickCount);
                model.onStartStop();
            }
        };
    }

    /**
     * Updates the seconds in the UI.
     * @param time
     */
    public void updateTime(final int time) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(() -> {
            final TextView tvS = (TextView) findViewById(R.id.seconds);
            final int seconds = time;
            tvS.setText(Integer.toString(seconds / 10) + Integer.toString(seconds % 10));
        });
    }

    /**
     * Updates the state name in the UI.
     * @param stateId
     */
    public void updateState(final int stateId) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread((new Runnable() {
            @Override
            public void run() {
                final TextView stateName = (TextView) findViewById(R.id.stateName);
                stateName.setText(getString(stateId));
            }

            }));
    }

    /**
     * Runs every time the button is pressed.
     * @param view
     */
    public void onStartStop(final View view) {

        countdown.cancel();

        counter = (TextView) findViewById(R.id.seconds);

        if(clickCount < 99) {
            clickCount++;
        }
        else    {
            clickCount = 99;
        }

        counter.setText(clickCount.toString());

        if(clickCount != 0) {
            countdown.start();
        }

        if(clickCount == 99)    {
            countdown.cancel();
            clickCount = 0;
            model.onStartStop();
        }

    }

    /**
     * Returns the amount of times the button has been pressed.
     * @return  An int
     */
    public int getClickCount()  {
        return clickCount;
    }

}
