package edu.luc.etl.cs313.android.simplestopwatch.model.state;
import edu.luc.etl.cs313.android.simplestopwatch.android.StopwatchAdapter;
import android.os.CountDownTimer;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class RunningState implements StopwatchState {

    public RunningState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    /**
     * Whenever the button is pressed this is ran
     */
    @Override
    public void onStartStop() {
        sm.actionStop();
        sm.toStoppedState();
    }

    /**
     * Whenever a second has passed this is ran.
     */
    @Override // Click counter
    public void onTick() {
        sm.actionInc();
        sm.toRunningState();
    }

    /**
     * Whenever an element changes on the screen this is ran.
     */
    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    /**
     * Returns the id of the current state
     * @return  A String
     */
    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}
